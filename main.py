from interceptor import Interceptor 

interceptor = Interceptor()
agent = interceptor.asnemo_agent

if __name__ == "__main__":
    print("Starting AsnemoAgent interceptor...")
    interceptor.start()
