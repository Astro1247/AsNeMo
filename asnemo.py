import base64
import time
import uuid
import zlib
import hashlib

from scapy.fields import ByteField, StrField, StrLenField
from scapy.packet import Packet
from scapy.sendrecv import sendp


class AsnemoPacket(Packet):
    fields_desc = [
        ByteField("mac_dst1", 0xff),
        ByteField("mac_dst2", 0xff),
        ByteField("mac_dst3", 0xff),
        ByteField("mac_dst4", 0xff),
        ByteField("mac_dst5", 0xff),
        ByteField("mac_dst6", 0xff),
        ByteField("agnt_prefix1", 0x1a),
        ByteField("agnt_prefix2", 0x2b),
        ByteField("agnt_prefix3", 0x3c),
        ByteField("agnt_src1", 0x1a),
        ByteField("agnt_src2", 0x2b),
        ByteField("agnt_src3", 0x3c),
        ByteField("packet_type1", 0x08),
        ByteField("packet_type2", 0x06),
        ByteField("agnt_src4", 0x00),
        ByteField("agnt_src5", 0x00),
        ByteField("agnt_src6", 0x00),
        ByteField("packet_num_1", 0x00),
        ByteField("packet_num_2", 0x00),
        ByteField("packet_total_1", 0x00),
        ByteField("packet_total_2", 0x00),
        StrField("agent_packet_type", "\x00ASNEMO\x00"),
        StrLenField("data", "", length_from=lambda pkt: pkt.len - 4),
    ]
    hash = None
    timestamp = None
    parsed = False

    def parse(self, pdata):
        if len(pdata) < 239:
            raise Exception("ASNEMO packet too short")
        self.mac_dst1 = int(pdata[0:2], 16)
        self.mac_dst2 = int(pdata[2:4], 16)
        self.mac_dst3 = int(pdata[4:6], 16)
        self.mac_dst4 = int(pdata[6:8], 16)
        self.mac_dst5 = int(pdata[8:10], 16)
        self.mac_dst6 = int(pdata[10:12], 16)
        self.agnt_prefix1 = int(pdata[12:14], 16)
        self.agnt_prefix2 = int(pdata[14:16], 16)
        self.agnt_prefix3 = int(pdata[16:18], 16)
        self.agnt_src1 = int(pdata[18:20], 16)
        self.agnt_src2 = int(pdata[20:22], 16)
        self.agnt_src3 = int(pdata[22:24], 16)
        self.packet_type1 = int(pdata[24:26], 16)
        self.packet_type2 = int(pdata[26:28], 16)
        self.agnt_src4 = int(pdata[28:30], 16)
        self.agnt_src5 = int(pdata[30:32], 16)
        self.agnt_src6 = int(pdata[32:34], 16)
        self.packet_num_1 = int(pdata[34:36], 16)
        self.packet_num_2 = int(pdata[36:38], 16)
        self.packet_total_1 = int(pdata[38:40], 16)
        self.packet_total_2 = int(pdata[40:42], 16)
        self.agent_packet_type = pdata[42:58]

        if self.agent_packet_type != b'0041534e454d4f00':
            raise Exception("Not ASNEMO packet")

        if pdata[58:64] == '485300' and pdata[192:200] == '00484500':
            self.hash = pdata[64:192]
        else:
            self.hash = None
            raise Exception("ASNEMO packet hash not found")

        if pdata[200:208] == '54535300' and pdata[208:228].isdigit() and pdata[228:238] == '0054534500':
            self.timestamp = int(''.join([chr(int(pdata[i:i + 2], 16)) for i in range(208, 228, 2)]))
        else:
            self.timestamp = None
            raise Exception("ASNEMO packet timestamp not found")
        self.data = pdata[238:]
        self.parsed = True
        return self

    def get_agent_src(self):
        return ':'.join([hex(i)[2:].zfill(2) for i in
                         [self.agnt_src1, self.agnt_src2, self.agnt_src3, self.agnt_src4, self.agnt_src5,
                          self.agnt_src6]])

    def get_packet_num(self):
        return (self.packet_num_1 << 8) + self.packet_num_2

    def get_packet_total(self):
        return (self.packet_total_1 << 8) + self.packet_total_2

    def get_packet_hash(self):
        return self.hash

    def get_packet_timestamp(self):
        return self.timestamp

    def get_data(self):
        return self.data


class AsnemoPacketReceiver(object):
    instance = None
    packets = []
    ttl = 300
    last_cleanup = 0
    cleanup_interval = ttl / 2
    data_hashes = []

    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super(AsnemoPacketReceiver, cls).__new__(cls)
        return cls.instance

    def __init__(self, ttl=5, cleanup_interval=5):
        self.ttl = ttl
        self.cleanup_interval = cleanup_interval

    def cleanup_packets(self):
        if time.time() - self.last_cleanup < self.cleanup_interval:
            return
        self.last_cleanup = time.time()
        cleaned_packets = [packet for packet in self.packets if packet['received'] + self.ttl > time.time()]
        removed_packets = [packet for packet in self.packets if packet['received'] + self.ttl <= time.time()]
        data_hashes = list(set([p['packet'].get_packet_hash() for p in removed_packets]))
        for data_hash in data_hashes:
            got_packets = [p['packet'] for p in removed_packets if p['packet'].get_packet_hash() == data_hash]
            got_packets_nums = [p.get_packet_num() for p in got_packets]
            if len(got_packets) == 0:
                continue
            total_packets = got_packets[0].get_packet_total()
            missing_packets = [i for i in range(0, total_packets) if i not in got_packets_nums]
            if len(missing_packets) == 0:
                continue
            print(f"Got {len(got_packets)} packets of {total_packets} total packets.")
            print(f"Missing packets: {missing_packets}")
        self.packets = cleaned_packets
        self.data_hashes = [dh for dh in self.data_hashes if dh['received'] + self.ttl > time.time()]

    def receive_packet(self, packet, merge_callback=None, send_callback=None
, transmit_hosts_callback=None):
        packet_obj = AsnemoPacket()
        try:
            packet_obj.parse(packet.original.hex())
        except Exception as e:
            print("Invalid ASNEMO packet")
        if packet_obj.get_agent_src() == AsnemoAgent().get_agent_src():
            return
        if packet_obj.get_packet_hash() in [p['hash'] for p in self.data_hashes]:
            return
        if len([p for p in self.packets if p['packet'].get_packet_num() == packet_obj.get_packet_num() and p[
            'packet'].get_packet_total() == packet_obj.get_packet_total() and p[
                                               'packet'].get_packet_hash() == packet_obj.get_packet_hash()]) > 0:
            return
        self.packets.append(
            {
                'packet': packet_obj,
                'received': time.time(),
            }
        )
        self.cleanup_packets()
        packet_obj.get_packet_num()
        total_packets = packet_obj.get_packet_total()
        received_packets = len([p for p in self.packets if
                                p['packet'].get_packet_timestamp() == packet_obj.get_packet_timestamp() and p[
                                    'packet'].get_packet_hash() == packet_obj.get_packet_hash()])
        if received_packets == total_packets:
            packets = sorted([p for p in self.packets if
                              p['packet'].get_packet_timestamp() == packet_obj.get_packet_timestamp() and p[
                                  'packet'].get_packet_hash() == packet_obj.get_packet_hash()],
                             key=lambda k: k['packet'].get_packet_num())
            data_bytes = b''
            for p in packets:
                data_bytes += p['packet'].get_data()
            data = ''.join([chr(int(data_bytes[i:i + 2], 16)) for i in range(0, len(data_bytes), 2)])
            agnt = AsnemoAgent()
            data = agnt.decode_data(agnt.decompress_data(data), packets[0]['packet'].agnt_src1)
            print(f"Successfuly got data from agent {packets[0]['packet'].get_agent_src()}")
            print(f"Packet age: {time.time() - packets[0]['packet'].get_packet_timestamp()}")
            self.data_hashes.append(
                {
                    'hash': packet_obj.get_packet_hash(),
                    'received': time.time(),
                }
            )
            if packets[0]['packet'].packet_type2 == 0x01:
                if merge_callback is not None:
                    merge_callback(data)
            elif packets[0]['packet'].packet_type2 == 0x03:
                params = {}
                for elem in data.split('&'):
                    split = elem.split('=')
                    params[split[0]] = split[1]
                if params['action'] == 'request' and params['data'] == 'hosts':
                    if transmit_hosts_callback is not None:
                        transmit_hosts_callback()
                    else:
                        print(f"Got request for hosts, but no send callback is set.")
            return data


class AsnemoAgent(object):
    instance = None
    agent_uuid = None

    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super(AsnemoAgent, cls).__new__(cls)
        return cls.instance

    def __init__(self, agent_uuid=None):
        if self.agent_uuid:
            return
        try:
            if agent_uuid is None:
                with open('/etc/machine-id', 'r') as f:
                    machine_id = f.read().strip()
                self.agent_uuid = uuid.UUID(machine_id)
            else:
                self.agent_uuid = uuid.UUID(agent_uuid)
        except:
            # Get windows machine id
            try:
                import winreg
                with winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, r'SOFTWARE\Microsoft\Cryptography', 0,
                                    winreg.KEY_READ | winreg.KEY_WOW64_64KEY) as key:
                    machine_id = winreg.QueryValueEx(key, 'MachineGuid')[0]
                    self.agent_uuid = uuid.UUID(machine_id)
            except:
                raise Exception("Failed to initialize agent UUID.")
        print(f"Agent UUID: {self.agent_uuid}")
        self.mac_address = [0x1a, 0x2b, 0x3c, 0x1a, 0x2b, 0x3c]
        for i in range(0, 6):
            self.mac_address[i] = (self.agent_uuid.int >> (8 * i)) & 0xff
        for i in range(0, 6):
            if self.mac_address[i] == 0x00:
                self.mac_address[i] = 0x01
            if self.mac_address[i] == 0xff:
                self.mac_address[i] = 0xfe
        print(f"Agent MAC: {':'.join([hex(i)[2:].zfill(2) for i in self.mac_address])}")

    def get_agent_src(self):
        return ':'.join([hex(i)[2:].zfill(2) for i in
                         [self.mac_address[0], self.mac_address[1], self.mac_address[2], self.mac_address[3],
                          self.mac_address[4], self.mac_address[5]]])

    def get_packet(self, reply=False, request=False):

        packet = AsnemoPacket(
            agnt_src1=self.mac_address[0],
            agnt_src2=self.mac_address[1],
            agnt_src3=self.mac_address[2],
            agnt_src4=self.mac_address[3],
            agnt_src5=self.mac_address[4],
            agnt_src6=self.mac_address[5],
            packet_type1=0xAE,
            packet_type2=0x01,
        )
        if reply:
            packet.packet_type2 = 0x02
        elif request:
            packet.packet_type2 = 0x03
        packet.sender_mac = ':'.join([hex(i)[2:].zfill(2) for i in self.mac_address])
        return packet

    def encode_data(self, data: str):
        data = ''.join([chr(ord(i) + self.mac_address[0]) for i in data])
        data = base64.b64encode(data.encode('utf-8')).decode('utf-8')
        return data

    def decode_data(self, data: str, offset: int):
        data = base64.b64decode(data.encode('utf-8')).decode('utf-8')
        data = ''.join([chr(ord(i) - offset) for i in data])
        return data

    def compress_data(self, data: str):
        data = zlib.compress(data.encode('utf-8'))
        data = base64.b64encode(data).decode('utf-8')
        return data

    def decompress_data(self, data: str):
        data = zlib.decompress(base64.b64decode(data.encode('utf-8'))).decode('utf-8')
        return data

    def generate_hash(self, data: str):
        hash = hashlib.sha256(data.encode('utf-8')).hexdigest()
        return hash

    def send_packet(self, data="", reply=False, request=False):
        if reply and request:
            raise Exception("Reply and request flags cannot be set at the same time")
        if data == "" and reply or request:
            data = "service=asnemo&action=request&data=hosts"
        packets_to_send = []
        timestamp = int(time.time())
        hash = self.generate_hash(data)
        initial_data_length = len(data)
        data = self.encode_data(data)
        encoded_data_length = len(data)
        data = self.compress_data(data)
        compressed_data_length = len(data)
        total_packets = len(data) // 1000 + 1
        for i in range(0, len(data), 1000):
            packet = self.get_packet(reply, request)
            packet.packet_total_1 = (total_packets >> 8) & 0xff
            packet.packet_total_2 = total_packets & 0xff
            packet.packet_num_1 = ((i // 1000) >> 8) & 0xff
            packet.packet_num_2 = (i // 1000) & 0xff
            data_to_send = f"HS\x00{hash}\x00HE\x00TSS\x00{timestamp}\x00TSE\x00{data[i:i + 1000]}"
            if len(data_to_send) < 64:
                data_to_send = data_to_send + "\x00" * (64 - len(data_to_send))
            packet.data = data_to_send
            packets_to_send.append(packet)
        print(f"Sending {len(packets_to_send)} packets")
        packets_per_second = 47
        _packet_interval = 1 / packets_per_second
        _loop_count = 1
        _loop_interval = 0.5
        for i in range(_loop_count):
            start = time.time()
            for packet in packets_to_send:
                sendp(packet)
                time.sleep(_packet_interval)
            print(f"Sent {len(packets_to_send)} packets in {time.time() - start} seconds")
            time.sleep(_loop_interval)
