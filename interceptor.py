import json
import time
import os
import scapy.all as scapy
from scapy.layers import http
from scapy.layers.inet import IP
from scapy.layers.l2 import ARP
import re
from asnemo import AsnemoPacketReceiver, AsnemoAgent
import threading


def is_valid_ip(ip_str):
    ip_parts = [int(part) for part in ip_str.split('.')]
    if (ip_parts[0] == 10) or (ip_parts[0] == 172 and 16 <= ip_parts[1] <= 31) or (
            ip_parts[0] == 192 and ip_parts[1] == 168):
        return True
    else:
        return False


def is_valid_mac(mac_str):
    mac_parts = mac_str.split(':')
    if len(mac_parts) != 6:
        return False
    for part in mac_parts:
        try:
            int(part, 16)
        except ValueError:
            return False
    if int(mac_parts[0], 16) & 1 == 1:
        return False
    if all(part == 'ff' for part in mac_parts):
        return False
    reserved_ouis = ['01:00:5e', '33:33']
    if any(mac_str.startswith(oui) for oui in reserved_ouis):
        return False
    return True


class Interceptor(object):
    instance = None
    _thread = None

    hosts = []
    last_saved = 0
    packets_receiver = None
    asnemo_agent = None

    asnemo_packet_prefix = "ffffffffffff1a2b3c.{6}ae0[123].{14}0041534e454d4f00485300.{128}0048450054535300\d{20}0054534500.*"
    prefix_pattern = re.compile(asnemo_packet_prefix)

    hosts_save_interval = 15
    last_hosts_save = 0

    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super(Interceptor, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self.packets_receiver = AsnemoPacketReceiver()
        self.asnemo_agent = AsnemoAgent()
        self.load_hosts()

    def merge_hosts(self, hosts):
        new_hosts = 0
        try:
            hosts = json.loads(hosts)
        except Exception as e:
            print(f"Failed to parse hosts into JSON: {e}")
            return
        for host in hosts:
            if host['mac'] not in [h['mac'] for h in self.hosts]:
                self.hosts.append(host)
                new_hosts += 1
            else:
                self.hosts[[h['mac'] for h in self.hosts].index(host['mac'])]['seen'] = host['seen']
                if 'ip' in host:
                    self.hosts[[h['mac'] for h in self.hosts].index(host['mac'])]['ip'] = host['ip']
        print(f"Received {len(hosts)} hosts, {new_hosts} new hosts, {len(hosts) - new_hosts} updated hosts")

    def transmit_hosts(self):
        sending_thread = threading.Thread(target=self.asnemo_agent.send_packet, args=(json.dumps(self.hosts),))
        sending_thread.start()

    def save_hosts(self):
        if time.time() - self.last_hosts_save > self.hosts_save_interval:
            print("Saving hosts")
            with open('hosts.json', 'w') as f:
                json.dump(self.hosts, f)
            print("Saved hosts")
            self.last_hosts_save = time.time()
            # Print total stats
            print("Total packets: " + str(sum([host['packets'] for host in self.hosts])))
            print("Total IP packets: " + str(sum([host['ip_packets'] for host in self.hosts])))
            print("Total ARP packets: " + str(sum([host['arp_packets'] for host in self.hosts])))
            print("Total HTTP packets: " + str(sum([host['http_packets'] for host in self.hosts])))
            print("Total hosts: " + str(len(self.hosts)))
            print("Total unique MACs: " + str(len(set([host['mac'] for host in self.hosts]))))
            for host in self.hosts:
                mac = host['mac']
                print(f"{mac} - "
                      f"{host['packets']} packets - "
                      f"{host['ip_packets']} IP packets - "
                      f"{host['arp_packets']} ARP packets - "
                      f"{host['http_packets']} HTTP packets - "
                      f"{host['ip'] if 'ip' in host else 'No IP'} - "
                      f"Known IPs: " + str(host['known_ips']) + " - "
                      f"{host['seen']} ({str(round(time.time() - host['seen']))} seconds ago)")

    def process_sniffed_packet(self, packet):
        try:
            if packet.haslayer(scapy.Raw) and self.prefix_pattern.match(packet.original.hex()):
                asnemo = True
                self.packets_receiver.receive_packet(
                    packet=packet,
                    merge_callback=self.merge_hosts,
                    send_callback=self.asnemo_agent.send_packet,
                    transmit_hosts_callback=self.transmit_hosts
                )
                return
            debug_pattern_regex = ".*0a141ffa0a141e31.*"
            debug_pattern = re.compile(debug_pattern_regex)
            if debug_pattern.match(packet.original.hex()):
                print('stop debug here')
            host_sender = {
                'mac': packet.src,
                'packets': 0,
                'ip_packets': 0,
                'arp_packets': 0,
                'http_packets': 0,
                'known_ips': [],
            }
            host_receiver = {
                'mac': packet.dst,
                'packets': 0,
                'ip_packets': 0,
                'arp_packets': 0,
                'http_packets': 0,
                'known_ips': [],
            }
            if host_receiver['mac'] == 'ff:ff:ff:ff:ff:ff' or host_receiver['mac'] == '00:00:00:00:00:00' or not is_valid_mac(host_receiver['mac']):
                host_receiver = None
            if packet.haslayer(IP):
                if is_valid_ip(packet[IP].src):
                    host_sender['ip'] = packet[IP].src
                    if packet[IP].src not in host_sender['known_ips']:
                        host_sender['known_ips'].append(packet[IP].src)
                host_sender['ip_packets'] += 1
                if host_receiver and is_valid_ip(packet[IP].dst):
                    host_receiver['ip'] = packet[IP].dst
                    if packet[IP].dst not in host_receiver['known_ips']:
                        host_receiver['known_ips'].append(packet[IP].dst)
                    host_receiver['ip_packets'] += 1
            if packet.haslayer(ARP):
                if is_valid_ip(packet[ARP].psrc):
                    host_sender['ip'] = packet[ARP].psrc
                    if packet[ARP].psrc not in host_sender['known_ips']:
                        host_sender['known_ips'].append(packet[ARP].psrc)
                host_sender['arp_packets'] += 1
                if host_receiver and is_valid_ip(packet[ARP].pdst):
                    host_receiver['ip'] = packet[ARP].pdst
                    if packet[ARP].pdst not in host_receiver['known_ips']:
                        host_receiver['known_ips'].append(packet[ARP].pdst)
                    host_receiver['arp_packets'] += 1
            if packet.haslayer(http.HTTPRequest):
                host_sender['http_packets'] += 1
                if host_receiver:
                    host_receiver['http_packets'] += 1
            host_sender['seen'] = packet.time
            if host_receiver:
                host_receiver['seen'] = packet.time
            if host_sender['mac'] not in [host['mac'] for host in self.hosts]:
                self.hosts.append(host_sender)
                mac = host_sender['mac']
                print(f"Discovered new host: {mac} - {host_sender['ip'] if 'ip' in host_sender else 'No IP'}")
            else:
                self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['seen'] = packet.time
                self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['packets'] += 1
                self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['ip_packets'] += host_sender[
                    'ip_packets']
                self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['arp_packets'] += host_sender[
                    'arp_packets']
                self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['http_packets'] += host_sender[
                    'http_packets']
                # Combine known IPs
                self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['known_ips'] = list(
                    set(self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['known_ips'] +
                        host_sender['known_ips']))
                if 'ip' in host_sender:
                    self.hosts[[host['mac'] for host in self.hosts].index(host_sender['mac'])]['ip'] = host_sender['ip']

            if host_receiver:
                if host_receiver['mac'] not in [host['mac'] for host in self.hosts]:
                    self.hosts.append(host_receiver)
                    mac = host_receiver['mac']
                    print(f"Discovered new host: {mac} - {host_receiver['ip'] if 'ip' in host_receiver else 'No IP'}")
                else:
                    self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['seen'] = packet.time
                    self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['packets'] += 1
                    self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['ip_packets'] += host_receiver[
                        'ip_packets']
                    self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['arp_packets'] += host_receiver[
                        'arp_packets']
                    self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['http_packets'] += host_receiver[
                        'http_packets']
                    self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['known_ips'] = list(
                        set(self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['known_ips'] +
                            host_receiver['known_ips']))
                    if 'ip' in host_receiver:
                        self.hosts[[host['mac'] for host in self.hosts].index(host_receiver['mac'])]['ip'] = host_receiver['ip']
        except Exception as e:
            print(f"Failed to process packet: {e}")
        try:
            self.save_hosts()
        except Exception as e:
            print(f"Failed to save hosts: {e}")

    def sniff(self, interface=None):
        while True:
            try:
                if interface:
                    scapy.sniff(iface=interface, store=False, prn=self.process_sniffed_packet)
                else:
                    scapy.sniff(store=False, prn=self.process_sniffed_packet)
            except Exception as e:
                print(e)
                time.sleep(1)

    def load_hosts(self):
        try:
            if not os.path.isfile('hosts.json'):
                print("Hosts file not found, creating...")
                with open('hosts.json', 'w') as f:
                    json.dump([], f)
            with open('hosts.json', 'r') as f:
                self.hosts = json.load(f)
        except Exception as e:
            print(e)

    def request_hosts(self):
        for _ in range(50):
            self.asnemo_agent.send_packet(request=True)
            time.sleep(0.002)

    def start(self, join=False):
        print("Starting interceptor...")
        if not self._thread:
            self._thread = threading.Thread(target=self.sniff)
            self._thread.start()
        else:
            print("Interceptor already started.")
        print("Done.")
        if join:
            self._thread.join()

    def join(self):
        if self._thread:
            self._thread.join()
        else:
            print("Interceptor not started.")
